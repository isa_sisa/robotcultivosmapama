﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA.Pruebas {
    class ContadorPlagasCultivo {
        public void totalPlagasCultivos() {
            ChromeDriver driver = new ChromeDriver();
            WebDriverWait wait = (new WebDriverWait(driver, TimeSpan.FromSeconds(60)));
            driver.Url = "http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/conaplipla.asp";

            //Hago click en el primer selector (ambito de la aplicación), para selccionar 'cultivos' .
            IWebElement element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(2) > p > select"));
            element.Click();
            element = element.FindElement(By.CssSelector("option:nth-child(2)")); element.Click();
            element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select > option:nth-child(2)")));
            element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select"));
            element.Click();
            IList<IWebElement> cultivos = element.FindElements(By.TagName("option"));
            int numcultivos = cultivos.Count;
            //Ahora voy seleccionando cada uno de los cultivos

            string filepath = Directory.GetParent(Directory.GetCurrentDirectory()).FullName + "\\prueba.txt";
            StreamWriter file = new StreamWriter(filepath, true);

            for (int i = 1; i<numcultivos;i++) {
                try {
                    element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select"));
                    element.Click();
                } catch { }
                cultivos = element.FindElements(By.TagName("option"));
                cultivos[i].Click();
                string nombrecultivo = cultivos[i].Text;
                element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#frmnombre > fieldset:nth-child(6) > p > select > option:nth-child(2)")));
                IWebElement selectorPlagas = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(6) > p > select"));
                IList<IWebElement> idsPlagas = selectorPlagas.FindElements(By.CssSelector("option"));
                int numplagas = idsPlagas.Count;

                var permissionSet = new PermissionSet(PermissionState.None);
                var writePermission = new FileIOPermission(FileIOPermissionAccess.Write, filepath);
                permissionSet.AddPermission(writePermission);

                if (permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet)) {
                    string textfile = nombrecultivo + " -> " + numplagas;
                    file.WriteLine(textfile);
                }

                driver.Url = "http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/conaplipla.asp";
                //Hago click en el primer selector (ambito de la aplicación), para selccionar 'cultivos' .
                element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(2) > p > select"));
                element.Click();
                element = element.FindElement(By.CssSelector("option:nth-child(2)")); element.Click();
                Thread.Sleep(3000);
                element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select > option:nth-child(2)")));
            }

            file.Close();
            driver.Quit();
        }
    }
}
