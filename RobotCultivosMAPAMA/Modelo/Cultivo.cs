﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA.Modelo {
    public class Cultivo {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public List<Plaga> Plagas { get; set; }

        public Cultivo(string id, string nombre) {
            this.Id = id;
            this.Nombre = nombre;
            this.Plagas = new List<Plaga>();
        }
    }
}
