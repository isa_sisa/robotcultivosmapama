﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA.Modelo {
    public class Plaga {
        public string Id { get; set; }
        public string Nombre { get; set; }

        public Plaga(string id, string nombre) {
            this.Id = id;
            this.Nombre = nombre;
        }
    }
}
