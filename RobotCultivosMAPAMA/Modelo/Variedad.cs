﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA.Modelo {
    public class Variedad {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public List<Plaga> Plagas { get; set; }

        public Variedad() {
            Plagas = new List<Plaga>();
        }

        public Variedad(string id, string nombre) {
            this.Id = id;
            this.Nombre = nombre;
            Plagas = new List<Plaga>();
        }
    }
}
