﻿using MySql.Data.MySqlClient;
using RobotCultivosMAPAMA.Modelo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA {

    public class MySqlAccess {
        public string[] parametros = ConfigurationManager.AppSettings["DataSourceMySql"].Split(',');
        public MySqlConnection connect = null;
        public MySqlDataReader Resultado = null;
        public string cadenaConexion;

        public MySqlAccess() { }
        MySqlConnection getConexion()
        {
            cadenaConexion = "Database= " + parametros[0] + "; Data Source= " + parametros[1] + "; User Id= " + parametros[2] + "; Password= " + parametros[3] + ";Pooling=false;Connection Lifetime=1; Max Pool Size=1";
            if (connect == null) {
                connect = new MySqlConnection(cadenaConexion);
                try {
                    connect.Open();
                } catch {
                    connect.Close();
                    connect.Dispose();
                }
            } else {
                if (connect.State == System.Data.ConnectionState.Closed) {
                    try {
                        connect.Open();
                    } catch {
                        connect.Close();
                        connect.Dispose();
                    }
                }
            }

            return connect;
        }

        void desconexion() {
            if (connect != null) {
                connect.Close();
                connect.Dispose();
            }
        }

        MySqlDataReader EjecutarSelectMysql(MySqlConnection Conn, string Select)//Metodo para ejecutar Select 
        {
            MySqlCommand ComandoSelect = new MySqlCommand(Select);
            ComandoSelect.Connection = Conn;
            try {
                Resultado = ComandoSelect.ExecuteReader();
            } catch (MySqlException ex) {
                throw new Exception(ex.Message);
            }

            return Resultado;
        }

        public bool AddCultivosPlagas(List<Cultivo> cultivos) {
            /*String queryCultivos = "INSERT IGNORE INTO cultivos (ID,Nombre) VALUES ";
            String queryPlagas = "INSERT IGNORE INTO plagas (ID,Nombre,Cultivo) VALUES ";

            //Consulta para los cultivos
            for (int i = 0; i < cultivos.Count(); i++) {
                queryCultivos += "('" + cultivos[i].Id + "','" + cultivos[i].Nombre + "')";
                if (i != (cultivos.Count() - 1)) queryCultivos += ",";
                else queryCultivos += ";";

                //Consulta para las plagas
                for (int j=0; j<cultivos[i].Plagas.Count();j++) {
                    queryPlagas += "('" + cultivos[i].Plagas[j].Id + "','" + cultivos[i].Plagas[j].Nombre + "','" + cultivos[i].Id + "')";
                    if ((i == (cultivos.Count() - 1)) && (j== (cultivos[i].Plagas.Count() - 1))) queryPlagas += ";";
                    else queryPlagas += ",";
                }
            }

            try {
                using (Resultado = EjecutarSelectMysql(getConexion(), queryCultivos)) { }
            } catch { return false; } finally { Resultado.Close(); Resultado.Dispose(); }
            try {
                using (Resultado = EjecutarSelectMysql(getConexion(), queryPlagas)) { }
            } catch { return false; } finally { Resultado.Close(); Resultado.Dispose(); }*/
            return true;
        }

        public bool AddFitosanitarios(List<Fitosanitario> fitosanitarios) {
            String query = "INSERT IGNORE INTO fitosanitarios (Variedad,Plaga,Producto) VALUES ";
            for (int i = 0; i < fitosanitarios.Count(); i++) {
                query += "('" + fitosanitarios[i].Cultivo.Id + "','" + fitosanitarios[i].Plaga.Id + "','" + fitosanitarios[i].Producto + "')";
                if (i != (fitosanitarios.Count() - 1)) query += ",";
                else query += ";";
            }

            try {
                using (Resultado = EjecutarSelectMysql(getConexion(), query)) { }
            } catch { return false; } finally { Resultado.Close(); Resultado.Dispose(); }
            return true;
        }

    }
}
