﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA.Modelo {
    public class Fitosanitario {
        public Cultivo Cultivo { get; set; }
        public Plaga Plaga { get; set; }
        public string Producto { get; set; }

        public Fitosanitario(Cultivo cultivo, Plaga plaga, string producto) {
            this.Cultivo = cultivo;
            this.Plaga = plaga;
            this.Producto = producto;
        }

    }
}
