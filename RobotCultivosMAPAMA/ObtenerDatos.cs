﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using RobotCultivosMAPAMA.Modelo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace RobotCultivosMAPAMA {
    class ObtenerDatos {

        private ChromeDriver driver = null;
        //private MySqlAccess mySqlAccess = new MySqlAccess();
        private List<Cultivo> cultivos = new List<Cultivo>();
        private EnviarDatosApi api = new EnviarDatosApi();
        private int port = Convert.ToInt32(ConfigurationManager.AppSettings["remotePORT"].ToString());
        private static string fechalog = DateTime.Now.ToString(@"yyyyMMddHHmmss");
        private string pathLogs = "log_" + fechalog + ".txt";

        public bool GetCultivosPlagas() {
            bool isAvailable = false;

            while (!isAvailable) {
                isAvailable = true;
                IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                List<TcpConnectionInformation> tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections().ToList();
                List<IPEndPoint> ipEndpoints = ipGlobalProperties.GetActiveTcpListeners().ToList();

                TcpConnectionInformation tci = tcpConnInfoArray.Where(t => t.LocalEndPoint.Port == port).FirstOrDefault();
                IPEndPoint ipep = ipEndpoints.Where(e => e.Port == port).FirstOrDefault();

                if (tci != null || ipep != null) {
                    isAvailable = false;
                    port++;
                }
            }

            string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
            ConfigurationManager.AppSettings["directoryPath"] = directoryPath;
            ProcessStartInfo psi = new ProcessStartInfo("cmd.exe", "/K java -jar " + ConfigurationManager.AppSettings["directoryPath"].ToString() + "/selenium-server-standalone-3.4.0.jar -Dwebdriver.chrome.driver=\"" + ConfigurationManager.AppSettings["directoryPath"].ToString() + "/chromedriver.exe\" -interactive -port " + Convert.ToString(port));
            Process process = Process.Start(psi);

            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(120);

            WebDriverWait wait = (new WebDriverWait(driver, TimeSpan.FromSeconds(10)));
            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                sw.WriteLine(DateTime.Now.ToString() + " WebDriver conectado");
            }

            try {
                int intentosCultivos = 0;
                bool cultivosRecogidos = false;
                IWebElement element = null;
                while (!cultivosRecogidos && intentosCultivos<9) {
                    try {
                        driver.Url = "http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/conaplipla.asp";
                        //Hago click en el primer selector (ambito de la aplicación), para selccionar 'cultivos'.
                        element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(2) > p > select"));
                        element.Click();
                        element = element.FindElement(By.CssSelector("option:nth-child(2)")); element.Click();
                        element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();

                        //Obtengo todos los ids de los cultivos que hay en el selector de cultivos
                        element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select"));
                        cultivosRecogidos = true;
                    } catch { }
                    intentosCultivos++;
                }
                
                IList<IWebElement> idsCultivos = element.FindElements(By.CssSelector("option"));
                for (int i = 1; i < idsCultivos.Count; i++) {
                    String nombre = idsCultivos[i].Text;
                    String id = idsCultivos[i].GetAttribute("value");
                    Cultivo cultivo = new Cultivo(id, nombre);
                    cultivos.Add(cultivo);
                }

                //TEST
                //cultivos = cultivos.Skip(0).Take(35).ToList();

                //Para cada uno de las especies tengo que recargar la página para obtener sus plagas.
                int cont = 2;

                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                    sw.WriteLine(DateTime.Now.ToString() + " NºCultivos = " + cultivos.Count);
                }

                for (int i=0; i<cultivos.Count; i++) {
                    /* bool plagasobtenidas = false;
                     int intentos = 0;
                     int numplagas = 0;
                     while (!plagasobtenidas && intentos <= 9) {
                         try {
                             driver.Url = "http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/conaplipla.asp";
                             //Hago click en el primer selector (ambito de la aplicación), para selccionar 'cultivos'.
                             element = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(2) > p > select"));
                             element.Click();
                             element = element.FindElement(By.CssSelector("option:nth-child(2)")); element.Click();
                             element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();
                             IWebElement selectorCultivos = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select"));
                             selectorCultivos.Click();

                             IWebElement cultivoAct = null;
                             try {
                                 cultivoAct = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select > option:nth-child(" + cont + ")"));
                             } catch { }
                             while (cultivoAct == null) {
                                 IList<IWebElement> totalCultivos = driver.FindElements(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select > option"));
                                 if (totalCultivos.Count < 2) {
                                     element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();
                                 }
                                 selectorCultivos = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select"));
                                 selectorCultivos.Click();
                                 cultivoAct = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(4) > p:nth-child(1) > select > option:nth-child(" + cont + ")"));
                             }
                             cultivoAct.Click();
                             element = driver.FindElement(By.CssSelector("#bt_matr1")); element.Click();

                             try {
                                 //Obtengo todas los ids de las plagas que hay en el selector de plagas
                                 IWebElement selectorPlagas = driver.FindElement(By.CssSelector("#frmnombre > fieldset:nth-child(6) > p > select"));
                                 IList<IWebElement> idsPlagas = null;
                                 idsPlagas = selectorPlagas.FindElements(By.CssSelector("option"));
                                 if (idsPlagas != null && (idsPlagas.Count > 1 || intentos > 9)) {
                                     plagasobtenidas = true;
                                     numplagas = idsPlagas.Count;
                                 }
                                 intentos++;
                                 for (int k = 1; k < idsPlagas.Count; k++) {
                                     String nombre = idsPlagas[k].Text;
                                     String id = idsPlagas[k].GetAttribute("value");
                                     if (!cultivos[i].Plagas.Exists(p => p.Id == id)) {
                                         cultivos[i].Plagas.Add(new Plaga(id, nombre));
                                     }
                                 }
                             } catch (Exception ex) {
                                 File.AppendAllText(pathLogs, DateTime.Now.ToString() + " ERROR AL OBTENER LOS IDS DE LAS PLAGAS PARA EL CULTIVO '" + cultivos[i].Nombre + "' -> " + ex.Message + Environment.NewLine);
                             }
                         } catch (Exception ex) {
                             File.AppendAllText(pathLogs, DateTime.Now.ToString() + " ERROR AL OBTENER LAS PLAGAS PARA EL CULTIVO '" + cultivos[i].Nombre + "' -> " + ex.Message + Environment.NewLine);
                         }
                     }

                     File.AppendAllText(pathLogs, DateTime.Now.ToString() + " RECOGIDAS "+ numplagas + " PLAGAS DEL CULTIVO '" + cultivos[i].Nombre + "'"+ Environment.NewLine);*/

                    try {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/conaplipla.asp");
                        request.Method = "POST";
                        request.ContentType = "application/x-www-form-urlencoded";
                        string postData = "estado=1&ambUti=01&culUso=" + cultivos[i].Id + "&ap=1&plagEfecto=-1&bt_matr1=Consultar";
                        byte[] bytes = Encoding.UTF8.GetBytes(postData);
                        request.ContentLength = bytes.Length;

                        Stream requestStream = request.GetRequestStream();
                        requestStream.Write(bytes, 0, bytes.Length);

                        WebResponse response = request.GetResponse();
                        Stream stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);

                        var resultPost = reader.ReadToEnd();
                        stream.Dispose();
                        reader.Dispose();

                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(resultPost);
                        HtmlNodeCollection optionPlagas = doc.DocumentNode.SelectNodes("//*[@id='frmnombre']/fieldset[3]/p/select/option");
                        if (optionPlagas != null && optionPlagas.Count > 0) {
                            optionPlagas.Remove(0);
                            foreach (HtmlNode op in optionPlagas) {
                                String nombrePlg = op.NextSibling.InnerText;
                                String idPlg = op.GetAttributeValue("value", "0");
                                cultivos[i].Plagas.Add(new Plaga(idPlg, nombrePlg));
                            }
                        }
                        using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                            sw.WriteLine(DateTime.Now.ToString() + " RECOGIDAS " + (optionPlagas.Count - 1) + " PLAGAS DEL CULTIVO '" + cultivos[i].Nombre + "'");
                        }
                    } catch (Exception ex){
                        using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                            sw.WriteLine(DateTime.Now.ToString() + " ERROR AL OBTENER LAS PLAGAS PARA EL CULTIVO '" + cultivos[i].Nombre + "' -> " + ex.Message);
                        }
                    }
                    cont++;
                }

            } catch (Exception ex){
                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                    sw.WriteLine(DateTime.Now.ToString() + " ERROR GENERAL EN LA RECOGIDA -> " + ex.Message);
                }
            }
            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                sw.WriteLine(DateTime.Now.ToString() + " CULTIVOS Y PLAGAS RECOGIDOS ");
            }

            List<Fitosanitario> fitosanitarios = GetFitosanitarios();

            driver.Quit();
            process.CloseMainWindow();
            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                sw.WriteLine(DateTime.Now.ToString() + " RECOGIDA DE DATOS TERMINADA");
            }

            bool result;
            try {
                result = api.EnviarCultivos(cultivos, fechalog);
                //if (result)
                    result &= api.EnviarFitosanitarios(fitosanitarios,fechalog);
            } catch(Exception ex) {
                Debug.WriteLine(ex.StackTrace);
                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                    sw.WriteLine(DateTime.Now.ToString() + " ERROR AL ENVIAR CULTIVOS Y FITOSANITARIOS -> " + ex.Message);
                }
                return false;
            }

            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                sw.WriteLine(DateTime.Now.ToString() + " EJECUCIÓN TERMINADA");
            }
            return result;
        }

        public List<Fitosanitario> GetFitosanitarios() {
            List<Fitosanitario> fitosanitarios = new List<Fitosanitario>();

            if (cultivos.Count > 0) {
                //Recorro todos los cultivos, y para cada cultivo recorro todas las plagas
                //Así obtengo los productos fitosanitarios de cada cultivo relacionado con cada plaga
                foreach (Cultivo cultivo in cultivos) {
                    foreach (Plaga plaga in cultivo.Plagas) {
                        int numFitosanitarios = 0;
                        bool recogidos = false;
                        int intentos = 0;
                        while (!recogidos && intentos < 5) {
                            try {
                                driver.Url = "http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/forexi.asp?e=0&plagEfecto=" + plaga.Id + "&culUso=" + cultivo.Id + "&ambUti=01&solEsp=";
                                //Compruebo si hay fitosanitarios para la variedad y la plaga seleccionados
                                IWebElement element = null;
                                try {
                                    element = driver.FindElement(By.CssSelector("#content > div:nth-child(3) > div"));
                                    if (element.Text != "" && element.Text.Contains("No existen datos")) {
                                        break;
                                        }
                                } catch { }
                                WebDriverWait wait = (new WebDriverWait(driver, TimeSpan.FromSeconds(60)));
                                element = driver.FindElement(By.CssSelector("#content > div:nth-child(2)"));
                                String[] inf = element.Text.Split(' ');
                                String nombrePlaga = inf[inf.Length - 1];
                                plaga.Nombre = nombrePlaga;
                                bool posteriores = true;
                                while (posteriores) {
                                    List<Fitosanitario> auxFitos = GetFitosanitariosVariedadPlaga(cultivo, plaga);
                                    numFitosanitarios += auxFitos.Count;
                                    fitosanitarios.AddRange(auxFitos);
                                    posteriores = false;
                                    IList<IWebElement> paginas = null;
                                    try {
                                        IList<IWebElement> elements = driver.FindElements(By.CssSelector("#content .documents"));
                                        if (elements.Count >= 6 && elements[5].Text.Contains("resultados")) {
                                            paginas = elements[5].FindElements(By.CssSelector("div > a"));
                                        }
                                    } catch { }
                                    if (paginas != null && paginas.Count > 0) {
                                        int pg = paginas.Count;
                                        for (int i = 0; i < pg; i++) {
                                            try {
                                                IList<IWebElement> elements = driver.FindElements(By.CssSelector("#content .documents"));
                                                paginas = elements[5].FindElements(By.CssSelector("div > a"));
                                            } catch { }
                                            try {
                                                if (!paginas[i].Text.Contains("anteriores")) {
                                                    if (paginas[i].Text.Contains("posteriores")) {
                                                        posteriores = true;
                                                        paginas[i].Click();
                                                    } else {
                                                        paginas[i].Click();
                                                        wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#content > div.contenedor_simple_tabla > div > table")));
                                                        auxFitos = GetFitosanitariosVariedadPlaga(cultivo, plaga);
                                                        numFitosanitarios += auxFitos.Count;
                                                        fitosanitarios.AddRange(auxFitos);
                                                    }
                                                }
                                            } catch { }
                                        }
                                    }
                                }
                                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                                    sw.WriteLine(DateTime.Now.ToString() + " RECOGIDOS " + numFitosanitarios + " FITOSANITARIOS DEL CULTIVO '" + cultivo.Nombre + "' Y LA PLAGA '" + plaga.Nombre + "'");
                                }
                                recogidos = true;
                            } catch (Exception ex) {
                                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                                    sw.WriteLine(DateTime.Now.ToString() + " ERROR AL CAMBIAR DE PÁGINA EN FITOSANITARIOS DEL CULTIVO '" + cultivo.Nombre + "' Y LA PLAGA '" + plaga.Nombre + "' -> " + ex.Message);
                                }
                                try {
                                    driver.Navigate().GoToUrl("http://www.mapama.gob.es/es/agricultura/temas/sanidad-vegetal/productos-fitosanitarios/registro/productos/forexi.asp?e=0&plagEfecto=" + plaga.Id + "&culUso=" + cultivo.Id + "&ambUti=01&solEsp=");
                                } catch { }
                            }
                            intentos++;
                        }
                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                sw.WriteLine(DateTime.Now.ToString() + " Fitosanitarios Recogidos = " + fitosanitarios.Count);
            }
            return fitosanitarios;
        }

        private List<Fitosanitario> GetFitosanitariosVariedadPlaga(Cultivo cultivo, Plaga plaga) {
            List<Fitosanitario> fitosanitarios = new List<Fitosanitario>();
            try {
                IList<IWebElement> tabla = driver.FindElements(By.CssSelector("#content > div.contenedor_simple_tabla > div > table > tbody > tr"));
                for (int i = 1; i < tabla.Count; i++) {
                    IWebElement prod = tabla[i].FindElement(By.CssSelector("td:nth-child(2) > span > a"));
                    string producto = prod.Text;
                    if (producto != null && producto != "") {
                        fitosanitarios.Add(new Fitosanitario(cultivo, plaga, producto));
                    }
                }
            } catch (Exception ex){
                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), pathLogs), true)) {
                    sw.WriteLine(" ERROR EN FITOSANITARIOS DEL CULTIVO '" + cultivo.Nombre + "' Y LA PLAGA '" + plaga.Nombre + "' -> " + ex.Message);
                }
            }
            return fitosanitarios;
        }

        private static string RemoveAccents(string s) {
            Encoding destEncoding = Encoding.GetEncoding("iso-8859-8");

            return destEncoding.GetString(
                Encoding.Convert(Encoding.UTF8, destEncoding, Encoding.UTF8.GetBytes(s)));
        }

    }
}
